#!/bin/bash

function Install()
{

OS=$(cat /etc/os-release | grep "^NAME" | awk -F'"' '{print $2}')

if [ $OS == "Ubuntu" ]
 then 
    echo "Installing nginx on Ubuntu"
    sudo apt-get install nginx -y

    #check if nginx services are up
    systemctl status nginx
elif [ $OS == "RedHat" ]
then
   echo "Installing nginx on RedHat"
   yum install nginx -y

    #check if nginx services are up
    service nginx start
else
  echo "OS not supported"
fi
}

function Uninstall()
{
sudo apt-get purge nginx nginx-common
sudo apt-get autoremove
}

action=$1
 case $action in
Install)
if [ -x /usr/sbin/nginx ]
  then
    echo "Nginx is already Installed and running"
     systemctl status nginx
        exit
else
        Install
fi
;;
Uninstall)
        Uninstall
;;
*)
        echo "Please enter the arguments"
;;
esac
