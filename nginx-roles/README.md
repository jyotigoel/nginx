# Ansible Assignment-5 for Installing Nginx Tool

## Nginx Tool Play
ansible-playbook -i inventory play.yml

![Assignment-5_Ansible_Play](/uploads/d5a98f963ef87fa2af4b3b6e342fd5ae/Assignment-5_Ansible_Play.PNG)

## Output of Webpage

![Assignment-5_index_page](/uploads/ee976862e0669fc5429b50b88cc9d368/Assignment-5_index_page.PNG)
